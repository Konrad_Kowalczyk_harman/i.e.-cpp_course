#include "../headers/Cat.hpp"

Cat::Cat(int w, std::string n) : weight(w), name(n)
{

}

Cat::~Cat()
{
	std::cout<<name<<std::endl;
}

void Cat::meow(int howLoud)
{
	std::string temp = "";
	for(int i=1; i<=howLoud; i++)
	{
		temp = temp + "!";
	}

	std::cout<<temp<<std::endl;
}

std::string Cat::getName()
{
	return name;
}

void Cat::setName(std::string value)
{
	name = value;
}
