#include <iostream>
#include "../headers/Cat.hpp"

int main()
{
	Cat c1{2};
	std::cout<<c1.getName()<<std::endl;
	Cat *c2 = new Cat{2, "Ebenezer"};
	std::cout<<c2->getName()<<std::endl;
	for(int i=0; i<10; i++)
	{
		Cat c1{i};
	}
	delete c2;
	return 0;
}
