#ifndef CAT_HPP_
#define CAT_HPP_
#include <string>
#include <iostream>

class Cat
{
	int weight;
	std::string name;

public:
	void meow (int howLoud);
	Cat(int w, std::string n = "babel");
	std::string getName();
	void setName(std::string value);
	~Cat();
};
#endif
