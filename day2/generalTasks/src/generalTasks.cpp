#include <iostream>
#include <vector>
#include <ctime>
#include <cstdlib>
#include <string>
#include <algorithm>


int uSum(int arr[], int size, int &average)
{
    int sum = 0;
    int sumAverage = 0;
    for(int i=0; i<size; i++)
    {
        if(arr[i]>0)
        {
            sum = sum + arr[i];
        }
        sumAverage = sumAverage + arr[i];
    }
    average = sumAverage/size;

    return sum;
}


void reverseVector(std::vector<int> &vect, unsigned long value)
{
    int digit;
    do
    {
        digit = value % 10;
        vect.push_back(digit);
        value /= 10;
    }
    while (value > 0);
}


void removeSpace(std::string &value)
{
    value.erase(std::remove_if(value.begin(), value.end(), isspace), value.end());
}


void removeChars (std::string &value)
{
    value.replace(value.begin(), value.begin()+1, "");
    value.back() = '\0';
}


int main()
{
    std::cout << "Task1\n";
    int arr[] {1,-2,3,-8};
    int average = 0;
    std::cout << "Positive numbers' sum: " << uSum(arr, sizeof(arr)/sizeof(arr[0]), average) << std::endl;
    std::cout << "Average: " << average << std::endl;


    std::cout << "\n\nTask2\n";
    srand(time(NULL));
    unsigned long randomNumber = (std::rand() % 50) + 7;
    std::cout << "Random number: " << std::to_string(randomNumber) << std::endl;
    std::vector<int> vect;
    reverseVector(vect, randomNumber);
    std::cout << "Reverse number: ";
    for(int i : vect)
    {
        std::cout << i;
    }


    std::cout << "\n\nTask3\n";
    std::string stringToRemoveSpace;
    std::cout << "Enter random string: ";
    std::getline(std::cin, stringToRemoveSpace);
    removeSpace(stringToRemoveSpace);
    std::cout << "String after remove space: " << stringToRemoveSpace;


    std::cout << "\n\nTask4\n";
    std::string stringToRemoveChars;
    std::cout << "Enter random string: ";
    std::cin >> stringToRemoveChars;
    removeChars(stringToRemoveChars);
    std::cout << "String after remove first and last characters: " << stringToRemoveChars;
    return 0;
}
