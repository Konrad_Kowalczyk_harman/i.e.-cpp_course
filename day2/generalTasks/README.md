GENERAL

You get an array of numbers, return the sum of all of the positives ones.

Example [1,-4,7,12] => 1 + 7 + 12 = 20
Note: if there is nothing to sum, the sum is default to 0.

1. Return the average of the given array rounded down to its nearest integer.
2. Given a random unsigned long number, You have to return the digits of this   number within an array in reverse order.
3. remove the spaces from the string, then return the resultant string.
4. Your goal is to create a function that removes the first and last characters of a string. You're given one parameter, the original string. You don't have to worry with strings with less than two characters.
