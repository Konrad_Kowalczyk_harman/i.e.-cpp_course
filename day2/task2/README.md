Task2
Create base class for representing general shape. Create classes which derives from this class and provide methods to calculate perimeter and area. Make base class pure. Do the same as above, but using pointers: each calculation of perimeter/area should be done on the same pointer. Do the same, but object should be created with new()
