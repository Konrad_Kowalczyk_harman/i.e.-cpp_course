#include "../headers/Triangle.hpp"

Triangle::Triangle(double a, double b, double c) : a(a), b(b), c(c)
{

}

double Triangle::getA()
{
    return a;
}

double Triangle::getB()
{
    return b;
}

double Triangle::getC()
{
    return c;
}

void Triangle::setA(double value)
{
    a = value;
}

void Triangle::setB(double value)
{
    b = value;
}

void Triangle::setC(double value)
{
    c = value;
}

double Triangle::calculatePerimeter()
{
    return a+b+c;
}

double Triangle::calculateArea()
{
    double p = calculatePerimeter()/2;
    return sqrt(p*(p-a)*(p-b)*(p-c));
}
