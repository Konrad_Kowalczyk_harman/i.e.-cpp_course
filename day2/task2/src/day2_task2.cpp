#include <iostream>
#include "../headers/Shape.hpp"
#include "../headers/Square.hpp"
#include "../headers/Triangle.hpp"

int main()
{
    Square square1(2);
    Triangle triangle1(3, 3, 4);
    Shape *shape1 = &square1;
    std::cout << "Square area: " << shape1->calculateArea() << std::endl;
    std::cout << "Square perimeter: " << shape1->calculatePerimeter() << std::endl;

    shape1 = &triangle1;
    std::cout << "Triangle area: " << shape1->calculateArea() << std::endl;
    std::cout << "Triangle perimeter: " << shape1->calculatePerimeter() << std::endl;

    std::cout<<"\nObjects create with new\n\n";
    Square *square2 = new Square(5);
    Triangle *triangle2 = new Triangle(3, 3, 4);
    Shape *shape2 = *&square2;
    std::cout << "Square area: " << shape2->calculateArea() << std::endl;
    std::cout << "Square perimeter: " << shape2->calculatePerimeter() << std::endl;

    shape1 = &triangle1;
    std::cout << "Triangle area: " << shape2->calculateArea() << std::endl;
    std::cout << "Triangle perimeter: " << shape2->calculatePerimeter() << std::endl;

    delete square2;
    delete triangle2;
    return 0;
}
