#include "../headers/Square.hpp"

Square::Square(double a) : a(a)
{

}

double Square::getA()
{
    return a;
}

void Square::setA(double value)
{
    a = value;
}

double Square::calculatePerimeter()
{
    return 4*a;
}

double Square::calculateArea()
{
    return a*a;
}
