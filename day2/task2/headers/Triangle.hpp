#ifndef TRIANGLE_HPP_
#define TRIANGLE_HPP_
#include "Shape.hpp"
#include <cmath>

class Triangle : public Shape
{
private:
    double a;
    double b;
    double c;
public:
    Triangle(double a = 2.0, double b = 2.0, double c = 2.0);
    virtual ~Triangle() {};
    double calculatePerimeter();
    double calculateArea();
    double getA();
    double getB();
    double getC();
    void setA(double value);
    void setB(double value);
    void setC(double value);
};
#endif
