#ifndef SHAPE_HPP_
#define SHAPE_HPP_

class Shape
{
public:
    virtual ~Shape() {};
    virtual double calculatePerimeter() = 0;
    virtual double calculateArea() = 0;
};
#endif
