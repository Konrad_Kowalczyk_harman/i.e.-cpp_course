#ifndef SQUARE_HPP_
#define SQUARE_HPP_

#include "Shape.hpp"

class Square : public Shape
{
private:
    double a;
public:
    Square(double a = 4.0);
    virtual ~Square() {};
    double calculatePerimeter();
    double calculateArea();
    double getA();
    void setA(double value);
};
#endif
