#ifndef BLOCK_HPP_
#define BLOCK_HPP_
#include "Solid.hpp"

class Block : public Solid
{
private:
    double width;
    double length;
    double height;
    double mass;

public:
    Block(double arr[]);
    double getWidth();
    double getLength();
    double getHeight();
    double getMass();
    void setWidth(double value);
    void setLength(double value);
    void setHeight(double value);
    void setMass(double value);
    double volume();
    double surfaceArea();
    double density();
    virtual ~Block() {}
};
#endif
