#ifndef SPHERE_HPP_
#define SPHERE_HPP_

#include "Solid.hpp"
#include <cmath>

class Sphere
{
private:
    double radius;
    double mass;

public:
    Sphere(double arr[2]);
    virtual ~Sphere() {}
    double getRadius();
    void setRadius(double value);
    double volume();
    double surfaceArea();
    double density();
};
#endif
