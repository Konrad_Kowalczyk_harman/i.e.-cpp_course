#ifndef SOLID_HPP_
#define SOLID_HPP_

class Solid
{
public:
    virtual ~Solid() {}
    virtual double volume() = 0;
    virtual double surfaceArea() = 0;
    virtual double density() = 0;
};
#endif
