#include "../headers/Block.hpp"
#include "../headers/Sphere.hpp"
#include "../headers/Solid.hpp"
#include <iostream>

int main()
{
    double arr[4] {3.1, 2.5, 4.2, 4.0};
    Block *bl1 = new Block(arr);
    Solid *sol1 = *&bl1;
    std::cout << sol1->volume() << std::endl;
    return 0;
}
