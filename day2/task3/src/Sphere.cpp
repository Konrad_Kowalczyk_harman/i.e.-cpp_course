#include "../headers/Sphere.hpp"

Sphere::Sphere(double arr[])
{
    radius = arr[0];
    mass = arr[1];
}

double Sphere::getRadius()
{
    return radius;
}

void Sphere::setRadius(double value)
{
    radius = value;
}

double Sphere::volume()
{
    return (4*M_PI*radius*radius*radius)/3;
}

double Sphere::density()
{
    return mass/volume();
}

double Sphere::surfaceArea()
{
    return 4*M_PI*radius*radius;
}
