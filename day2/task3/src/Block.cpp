#include "../headers/Block.hpp"

Block::Block(double arr[])
{
    width = arr[0];
    length= arr[1];
    height = arr[2];
    mass = arr[3];
}

double Block::getHeight()
{
    return height;
}

double Block::getLength()
{
    return length;
}

double Block::getMass()
{
    return mass;
}

double Block::getWidth()
{
    return width;
}

void Block::setHeight(double value)
{
    height = value;
}

void Block::setLength(double value)
{
    length = value;
}

void Block::setMass(double value)
{
    mass = value;
}

void Block::setWidth(double value)
{
    width = value;
}

double Block::density()
{
    return mass/volume();
}

double Block::surfaceArea()
{
    return (2*width*length + 2*length*height + 2*width*height);
}

double Block::volume()
{
    return width*length*height;
}
