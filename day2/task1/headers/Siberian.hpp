#ifndef SIBERIAN_HPP_
#define SIBERIAN_HPP_

#include "Cat.hpp"

class Siberian : public Cat
{
private:
    int weight;
    std::string name;
public:
    Siberian(int w, std::string n);
    void meow(int howLoud) override;
    virtual ~Siberian() {};
    int getWeight();
    void setWeight(int value);
    std::string getName();
    void setName(std::string value);
};
#endif
