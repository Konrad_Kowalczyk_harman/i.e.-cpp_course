#ifndef NEVA_HPP_
#define NEVA_HPP_

#include "Cat.hpp"

class Neva : public Cat
{
private:
    int weight;
    std::string name;
public:
    Neva(int w, std::string n);
    void meow(int howLoud) override;
    virtual ~Neva() {};
    int getWeight();
    void setWeight(int value);
    std::string getName();
    void setName(std::string value);
};
#endif
