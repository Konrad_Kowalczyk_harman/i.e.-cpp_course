#ifndef CAT_HPP_
#define CAT_HPP_

#include <string>
#include <iostream>

class Cat
{
public:
    virtual void meow(int howLoud) = 0;
    virtual ~Cat() {};
};
#endif
