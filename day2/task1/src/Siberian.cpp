#include "../headers/Siberian.hpp"

Siberian::Siberian(int w, std::string n) : weight(w), name(n)
{

}

void Siberian::meow(int howLoud)
{
    std::string temp = "";
    for(int i=0; i<howLoud; i++)
    {
        temp = temp + "!";
    }
    std::cout << "\n" << temp;
}

std::string Siberian::getName()
{
    return name;
}

void Siberian::setName(std::string value)
{
    name = value;
}

int Siberian::getWeight()
{
    return weight;
}

void Siberian::setWeight(int value)
{
    weight = value;
}
