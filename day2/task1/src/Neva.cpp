#include "../headers/Neva.hpp"

Neva::Neva(int w, std::string n) : weight(w), name(n)
{

}

void Neva::meow(int howLoud)
{
    std::string temp = "";
    for(int i=0; i<howLoud; i++)
    {
        temp = temp + "?";
    }
    std::cout << "\n" << temp;
}

std::string Neva::getName()
{
    return name;
}

void Neva::setName(std::string value)
{
    name = value;
}

int Neva::getWeight()
{
    return weight;
}

void Neva::setWeight(int value)
{
    weight = value;
}
