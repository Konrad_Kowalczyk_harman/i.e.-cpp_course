#include "../headers/Cat.hpp"
#include "../headers/Siberian.hpp"
#include "../headers/Neva.hpp"

int main()
{
    Neva nev1(4, "Puszek");
    nev1.meow(1);

    Siberian sib1(3, "Garfield");
    sib1.meow(3);

    return 0;
}
