#include "../headers/Dolphin.hpp"

Dolphin::Dolphin(const std::string& nameConstr, int ageConstr, double massConstr) : Animal(nameConstr, ageConstr), mass(massConstr)
{

}

void Dolphin::setMass(double value)
{
    mass = value;
}

double Dolphin::getMass()
{
    return mass;
}

void Dolphin::display()
{
    std::cout << "\nName: " << Animal::getName() << "\nAge: " << Animal::getAge() << "\nMass: " << mass << std::endl;
}
