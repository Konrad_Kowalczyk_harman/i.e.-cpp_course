#include "../headers/Zebra.hpp"

Zebra::Zebra(const std::string& nameConstr, int ageConstr, int stripeNumberConstr) : Animal(nameConstr, ageConstr), stripeNumber(stripeNumberConstr)
{

}

void Zebra::setStripeNumber(int value)
{
    stripeNumber = value;
}

int Zebra::getStripeNumber()
{
    return stripeNumber;
}

void Zebra::display()
{
    std::cout << "\nName: " << Animal::getName() << "\nAge: " << Animal::getAge() << "\nStripe number: " << stripeNumber << std::endl;
}
