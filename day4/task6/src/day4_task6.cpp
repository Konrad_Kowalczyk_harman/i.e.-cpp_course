#include <iostream>
#include "../headers/Animal.hpp"
#include "../headers/Zebra.hpp"
#include "../headers/Dolphin.hpp"

int main()
{
    Dolphin dol("Orka", 3, 2000);
    Zebra zeb("Alex", 1, 150);
    dol.display();
    zeb.display();
    return 0;
}
