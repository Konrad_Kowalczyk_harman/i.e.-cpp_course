#include "../headers/Animal.hpp"

Animal::Animal(const std::string& nameConstr, int ageConstr) : name(nameConstr), age(ageConstr)
{

}

const int Animal::getAge()
{
    return age;
}

const std::string Animal::getName()
{
    return name;
}

void Animal::setAge(int value)
{
    age = value;
}

void Animal::setName(const std::string& value)
{
    name = value;
}
