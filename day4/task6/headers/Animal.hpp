#ifndef ANIMAL_HPP_
#define ANIMAL_HPP_
#include <string>
#include <iostream>

class Animal
{
private:
    std::string name;
    int age;
public:
    Animal(const std::string& nameConstr, int ageConstr);
    virtual ~Animal() {}
    void setName(const std::string& value);
    void setAge(int value);
    const std::string getName();
    const int getAge();
};
#endif
