#ifndef ZEBRA_HPP_
#define ZEBRA_HPP_

#include "Animal.hpp"

class Zebra : public Animal
{
private:
    int stripeNumber;
public:
    virtual ~Zebra() {}
    Zebra(const std::string& nameConstr, int ageConstr, int stripeNumberConstr);
    void setStripeNumber(int value);
    int getStripeNumber();
    void display();
};
#endif
