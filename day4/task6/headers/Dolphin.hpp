#ifndef DOLPHIN_HPP_
#define DOLPHIN_HPP_

#include "Animal.hpp"

class Dolphin : public Animal
{
private:
    double mass;
public:
    virtual ~Dolphin() {}
    Dolphin(const std::string& nameConstr, int ageConstr, double massConstr);
    void display();
    void setMass(double value);
    double getMass();
};
#endif
