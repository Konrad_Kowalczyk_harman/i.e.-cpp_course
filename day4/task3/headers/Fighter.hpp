#ifndef FIGHTER_HPP_
#define FIGHTER_HPP_
#include <string>
#include <iostream>
#include <ctime>

class Fighter
{
    std::string name;
    int health;
    int damageAttack;

public:
    Fighter(const std::string& nameConstruct, int healthConstruct, int damageAttackConstruct);
    const std::string getName();
    const int getHealth();
    const int getDamageAttack();
    void setHealth(int value);
};
#endif
