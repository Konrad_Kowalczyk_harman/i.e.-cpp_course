#include <iostream>
#include "../headers/Fighter.hpp"

void fight(Fighter& f1, Fighter& f2)
{
    int roundCounter = 1;
    do
    {
        std::cout << f1.getName() << " hits!" << std::endl;
        f2.setHealth(f2.getHealth() - f1.getDamageAttack());
        std::cout << f2.getName() << "' health: " << f2.getHealth() << std::endl;
        std::cout << f2.getName() << " hits!" << std::endl;
        f1.setHealth(f1.getHealth() - f2.getDamageAttack());
        std::cout << f1.getName() << "' health: " << f1.getHealth() << std::endl;
        std::cout << "Round is finished\n\n";
        roundCounter++;
    }
    while ((f2.getHealth() > 0) && (f1.getHealth() > 0) && (roundCounter < 12));

    if (f1.getHealth() > 0 && f1.getHealth() > f2.getHealth())
    {
        std::cout << "The winner is " << f1.getName() << std::endl;
    }
    else
    {
        std::cout << "The winner is " << f2.getName() << std::endl;
    }
}

int main()
{
    srand(time(NULL));
    Fighter f1("Golota", 100, 15);
    Fighter f2("Kliczko", 200, 9);
    fight(f1, f2);
    return 0;
}
