#include "../headers/Fighter.hpp"

Fighter::Fighter(const std::string& nameConstruct, int healthConstruct, int damageAttackConstruct) : name(nameConstruct), health(healthConstruct), damageAttack(damageAttackConstruct)
{

}

const std::string Fighter::getName()
{
    return name;
}

const int Fighter::getDamageAttack()
{
    int currentDamage  = (std::rand() % (damageAttack + 1)) + 0;
    if(currentDamage == 0)
    {
        std::cout << name << " miss opponent" << std::endl;
    }
    return currentDamage;
}

const int Fighter::getHealth()
{
    return health;
}

void Fighter::setHealth(int value)
{
    health = value;
}
