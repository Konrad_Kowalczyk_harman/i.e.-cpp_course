#ifndef MOTHER_HPP_
#define MOTHER_HPP_

#include <iostream>

class Mother
{
public:
    virtual ~Mother() {}
    void display();
    virtual void displayVirtual();
};
#endif
