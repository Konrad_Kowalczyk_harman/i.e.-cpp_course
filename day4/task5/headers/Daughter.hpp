#ifndef DAUGHTER_HPP_
#define DAUGHTER_HPP_

#include "../headers/Mother.hpp"

class Daughter : public Mother
{
public:
    virtual ~Daughter() {}
    void display();
    void displayVirtual();
};
#endif
