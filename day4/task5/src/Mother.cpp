#include "../headers/Mother.hpp"

void Mother::display()
{
    std::cout << "Mother" << std::endl;
}

void Mother::displayVirtual()
{
    std::cout << "Virtual Mother" << std::endl;
}
