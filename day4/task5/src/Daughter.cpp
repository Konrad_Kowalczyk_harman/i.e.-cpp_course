#include "../headers/Daughter.hpp"

void Daughter::display()
{
    std::cout << "Daughter" << std::endl;
}

void Daughter::displayVirtual()
{
    std::cout << "Virtual Daughter" << std::endl;
}
