#include <iostream>
#include "../headers/Mother.hpp"
#include "../headers/Daughter.hpp"


int main()
{
    Daughter dag;
    dag.display();
    dag.displayVirtual();

    Mother * mot = &dag;
    mot->display();
    mot->displayVirtual();
    return 0;
}
