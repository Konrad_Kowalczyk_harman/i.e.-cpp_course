#ifndef TRIANGLE_HPP_
#define TRIANGLE_HPP_

#include "Shape.hpp"

class Triangle : public Shape
{
public:
    Triangle(int widthConstruct = 3, int heightConstruct = 4);
    virtual ~Triangle() {}
    double area();
};
#endif
