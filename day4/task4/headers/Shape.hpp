#ifndef SHAPE_HPP_
#define SHAPE_HPP_

class Shape
{
private:
    int width;
    int height;
public:
    Shape(int widthConstruct, int heightConstruct);
    virtual ~Shape() {}
    int getWidth();
    int getHeight();
    void setWidth(int value);
    void setHeight(int value);
};
#endif
