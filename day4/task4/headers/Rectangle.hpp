#ifndef RECTANGLE_HPP_
#define RECTANGLE_HPP_

#include "Shape.hpp"

class Rectangle : public Shape
{
public:
    Rectangle(int widthConstruct = 5, int heightConstruct = 3);
    virtual ~Rectangle() {}
    double area();
};
#endif
