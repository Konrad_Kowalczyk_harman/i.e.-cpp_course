#include "../headers/Rectangle.hpp"

Rectangle::Rectangle(int widthConstruct, int heightConstruct) : Shape(widthConstruct, heightConstruct)
{

}

double Rectangle::area()
{
    return Shape::getHeight() * Shape::getWidth();
}

