#include <iostream>
#include "../headers/Shape.hpp"
#include "../headers/Rectangle.hpp"
#include "../headers/Triangle.hpp"

int main()
{
    Rectangle rec1(2, 3);
    Triangle tr1;

    std::cout << "Rectangle area: " << rec1.area() << std::endl;
    std::cout << "Triangle area: " << tr1.area() << std::endl;
    return 0;
}
