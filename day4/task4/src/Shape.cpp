#include "../headers/Shape.hpp"
Shape::Shape(int widthConstruct, int heightConstruct) : width(widthConstruct), height(heightConstruct)
{

}

int Shape::getHeight()
{
    return height;
}

int Shape::getWidth()
{
    return width;
}

void Shape::setHeight(int value)
{
    height = value;
}

void Shape::setWidth(int value)
{
    width = value;
}

