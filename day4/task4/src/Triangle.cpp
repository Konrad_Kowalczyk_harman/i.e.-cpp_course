#include "../headers/Triangle.hpp"

Triangle::Triangle(int widthConstruct, int heightConstruct) : Shape(widthConstruct, heightConstruct)
{

}

double Triangle::area()
{
    return Shape::getHeight()*Shape::getWidth() / 2;
}
