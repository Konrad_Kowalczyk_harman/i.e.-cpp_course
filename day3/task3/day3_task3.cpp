#include <iostream>

template <class T>
class Incrementer
{
private:
    T value;

public:
    Incrementer(T value)
    {
        this->value = value;
    }
    void increment(T number)
    {
        value = value + number;
    }
    T getValue()
    {
        return value;
    }
};

int main()
{
    Incrementer <int> incr1(5);
    std::cout<<"Example based on int\n";
    std::cout << "Private value before increment = " << incr1.getValue()<<std::endl;
    incr1.increment(4);
    std::cout << "Private value after increment by 4 = " << incr1.getValue()<<std::endl;

    Incrementer <double> incr2(5.5);
    std::cout<<"\nExample based on double\n";
    std::cout << "Private value before increment = " << incr2.getValue()<<std::endl;
    incr2.increment(4.2);
    std::cout << "Private value after increment by 4.2 = " << incr2.getValue()<<std::endl;

    Incrementer <short> incr3(-3);
    std::cout<<"\nExample based on short\n";
    std::cout << "Private value before increment = " << incr3.getValue()<<std::endl;
    incr3.increment(-1);
    std::cout << "Private value after increment by -1 = " << incr3.getValue()<<std::endl;

    Incrementer <uint> incr4(2);
    std::cout<<"\nExample based on uint\n";
    std::cout << "Private value before increment = " << incr4.getValue()<<std::endl;
    incr4.increment(4);
    std::cout << "Private value after increment by 4 = " << incr4.getValue()<<std::endl;
    return 0;
}
