#include <iostream>

template <typename T, typename P> T substract (T a, P b)
{
    return a - b;
}

int main()
{
    int a = -5, b = 10;
    uint c = 1, d = 3;
    double e = 1.5, f = 2.5;
    short g = 4, h = 2;
    std::cout << "Substract int - int: " << a << " - " << b << " = " << substract(a,b) << std::endl;
    std::cout << "Substract int - uint: " << b << " - " << c << " = " << substract(b,c) << std::endl;
    std::cout << "Substract uint - int: " << d << " - " << a << " = " << substract(d,a) << std::endl;
    std::cout << "Substract int - double: " << a << " - " << e << " = " << substract(a,e) << std::endl;
    std::cout << "Substract double - int: " << f << " - " << a << " = " << substract(f,a) << std::endl;
    std::cout << "Substract int - short: " << b << " - " << h << " = " << substract(b,h) << std::endl;
    std::cout << "Substract short - int: " << g << " - " << b << " = " << substract(g,b) << std::endl;
    std::cout << "Substract uint - uint: " << c << " - " << d << " = " << substract(c,d) << std::endl;
    std::cout << "Substract uint - double: " << d << " - " << e << " = " << substract(d,e) << std::endl;
    std::cout << "Substract double - uint: " << f << " - " << c << " = " << substract(f,c) << std::endl;
    std::cout << "Substract uint - short: " << d << " - " << g << " = " << substract(d,g) << std::endl;
    std::cout << "Substract short - uint: " << h << " - " << c << " = " << substract(h,c) << std::endl;
    std::cout << "Substract double - double: " << e << " - " << f << " = " << substract(e,f) << std::endl;
    std::cout << "Substract short - double: " << g << " - " << e << " = " << substract(g,e) << std::endl;
    std::cout << "Substract double - short: " << f << " - " << h << " = " << substract(f,h) << std::endl;
    std::cout << "Substract int - 100000: " << g << " - 100000" << " = " << substract(g,100000) << std::endl;
    std::cout << "Substract uint - 100000: " << g << " - 100000" << " = " << substract(g,100000) << std::endl;
    std::cout << "Substract double - 100000: " << g << " - 100000" << " = " << substract(g,100000) << std::endl;
    std::cout << "Substract short - 100000: " << g << " - 100000" << " = " << substract(g,100000) << std::endl;
    return 0;
}
