Task2
Create template function, which will substract two numbers. Try it with various combinations of types int, uint, double, short and with numbers which does not fit into other types (like 100000 does not fit to short). What type should be returned? Is it possible to cover all those cases with the same template? Add text file to commit with console output which shows your experiments.
