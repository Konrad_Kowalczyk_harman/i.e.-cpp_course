Task1
Use function std::stoi() to convert string to number. Find in the documentation, how this function will behave for differently formatted strings with numbers. What will happen if std::stoi is not able to parse string? Propose exception handling for such situation.
Add text file to commit with console output which shows your experiments.
