#include <iostream>
#include <string>

int main()
{
    std::string decimalString = "123";
    std::string hexStringPrefix = "0xfa";
    std::string hexString = "FA";
    std::string binString = "1100";
    std::string text = "96 year of my birthday";
    std::string exampleException1 = "World War 2";
    std::string exampleException2 = "10000000000";

    try
    {
        std::cout << "Number from string: 123 = " << std::stoi(decimalString) << std::endl;
        std::cout << "Number from string: 0xfa = " << std::stoi(hexString, nullptr, 16) << std::endl;
        std::cout << "Number from string: FA = " << std::stoi(hexString, nullptr, 16) << std::endl;
        std::cout << "Number from string: 1100 = " << std::stoi(binString, nullptr, 2) << std::endl;
        std::string::size_type st = 4;
        std::cout << "\nNumber from string: 96 year of my birthday = " << std::stoi(text) << std::endl;
        std::cout << "The above string with pointer on 5th character = " << text.substr(st) << std::endl;
        std::cout << "\nNumber from string: 96 year of my birthday = " << std::stoi(text, &st) << std::endl;
        std::cout << "The above string with pointer on first character after number = " << text.substr(st) << std::endl;
        std::cout << std::stoi(exampleException1) << std::endl;
        std::cout << std::stoi(exampleException2) << std::endl;
    }
    catch (const std::invalid_argument &e)
    {
        std::cout << "\nInvalid argument: " << e.what() << std::endl;
    }
    catch (const std::out_of_range &e)
    {
        std::cout << "\nOut of range: " << e.what() << std::endl;
    }
    catch(...)
    {
        std::cout << "\nUnknown exception\n";
    }

    return 0;
}
